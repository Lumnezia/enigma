var UKW;
(function (UKW) {
    UKW[UKW["I"] = 0] = "I";
    UKW[UKW["II"] = 1] = "II";
    UKW[UKW["III"] = 2] = "III";
})(UKW || (UKW = {}));
/**
 * Official wirings of the german enigma machine. The last one is used for testing and is wired one-to-one
 */
var UKW_WIRINGS = [
    [4, 9, 12, 25, 0, 11, 24, 23, 21, 1, 22, 5, 2, 17, 16, 20, 14, 13, 19, 18, 15, 8, 10, 7, 6, 3],
    [24, 17, 20, 7, 16, 18, 11, 3, 15, 23, 13, 6, 14, 10, 12, 8, 4, 1, 5, 25, 2, 22, 21, 9, 0, 19],
    [5, 21, 15, 9, 8, 0, 14, 24, 4, 3, 17, 25, 23, 22, 6, 2, 19, 10, 20, 16, 18, 1, 13, 12, 7, 11],
];
var Ukw = /** @class */ (function () {
    /**
     * Will create a new ukw and set the name and wiring
     * @param wiring The wiring to use
     */
    function Ukw(wiring) {
        this.wiring = UKW_WIRINGS[wiring];
        this.name = UKW[wiring];
    }
    /**
     * Will return the name of the ukw
     */
    Ukw.prototype.getName = function () {
        return this.name;
    };
    /**
     * Will return a converted character based on the wiring
     * @param char The character to convert
     */
    Ukw.prototype.sendInput = function (char) {
        return this.wiring[char];
    };
    return Ukw;
}());
//# sourceMappingURL=Ukw.js.map