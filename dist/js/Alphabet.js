var Alphabet = /** @class */ (function () {
    function Alphabet() {
    }
    Alphabet.convertCharacterToIndex = function (letter) {
        if (letter.length !== 1)
            return -1;
        var alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        return alphabet.search(letter.toUpperCase());
    };
    Alphabet.convertIndexToCharacter = function (index) {
        if (index < 0 || index > 25)
            return null;
        var alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        return alphabet[index];
    };
    return Alphabet;
}());
//# sourceMappingURL=Alphabet.js.map