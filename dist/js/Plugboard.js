var Plugboard = /** @class */ (function () {
    function Plugboard() {
        this.linked = [];
    }
    /**
     * Will create a link between two character if none is already linked
     * @param char1 The first character in the link
     * @param char2 The second cahracter in the link
     */
    Plugboard.prototype.addLink = function (char1, char2) {
        var duplicate = false;
        this.linked.forEach(function (link) {
            if (link[0] === char1 || link[1] === char1 || link[0] === char2 || link[1] === char2 || char1 === char2) {
                duplicate = true;
            }
        });
        if (!duplicate) {
            this.linked.push([char1, char2]);
        }
        else {
            console.warn("Plugboardlink [" + char1 + ", " + char2 + "] could not have been added, because at least one of the chars is already linked.");
        }
    };
    /**
     * Will convert a character based on the links set in the plugboard
     * @param char The character to send to the plugboard
     */
    Plugboard.prototype.sendInput = function (char) {
        var result = char;
        this.linked.forEach(function (link) {
            if (link[0] === char) {
                result = link[1];
            }
            else if (link[1] === char) {
                result = link[0];
            }
        });
        return result;
    };
    return Plugboard;
}());
//# sourceMappingURL=Plugboard.js.map