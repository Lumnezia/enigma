class Plugboard {
  private linked: number[][] = [];

  /**
   * Will create a link between two character if none is already linked
   * @param char1 The first character in the link
   * @param char2 The second cahracter in the link
   */
  public addLink(char1: number, char2: number) {
    let duplicate = false;
    this.linked.forEach((link) => {
      if (link[0] === char1 || link[1] === char1 || link[0] === char2 || link[1] === char2 || char1 === char2) {
        duplicate = true;
      }
    });
    if (!duplicate) {
      this.linked.push([char1, char2]);
    } else {
      console.warn(`Plugboardlink [${char1}, ${char2}] could not have been added, because at least one of the chars is already linked.`);
    }
  }

  /**
   * Will convert a character based on the links set in the plugboard
   * @param char The character to send to the plugboard
   */
  public sendInput(char: number): number {
    let result = char;
    this.linked.forEach((link) => {
      if (link[0] === char) {
        result = link[1];
      } else if (link[1] === char) {
        result = link[0];
      }
    });
    return result;
  }
}
