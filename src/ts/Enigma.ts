class Enigma {
  private rotors: Rotor[];
  private ukw: Ukw;
  private plugboard: Plugboard;

  /**
   * Create a new enigma machine and set its rotors and ukw
   * @param rotors The rotors to use
   * @param ukw The ukw to use
   * @param plugboard The plugboard to use
   */
  constructor(rotors: Rotor[], ukw: Ukw, plugboard?: Plugboard) {
    this.rotors = rotors;
    this.ukw = ukw;
    this.plugboard = plugboard;
  }

  /**
   * Sends a letter through all the rotors and returns the converted value
   * Will increase the position of the rotors afterwards
   * @param keyIndex The index of the letter to convert (A=0, B=1 and so on)
   */
  public encryptCharacter(char: string): string {
    let charIndex = Alphabet.convertCharacterToIndex(char);
    if (this.plugboard) {
      charIndex = this.plugboard.sendInput(charIndex);
    }

    let forward = this.rotors[0].sendForwardInput(charIndex);
    let forward2 = this.rotors[1].sendForwardInput(forward);
    let forward3 = this.rotors[2].sendForwardInput(forward2);

    let ukwResult = this.ukw.sendInput(forward3);

    let backward3 = this.rotors[2].sendBackwardsInput(ukwResult);
    let backward2 = this.rotors[1].sendBackwardsInput(backward3);
    let finalResult = this.rotors[0].sendBackwardsInput(backward2);
    this.increaseRotorPositions();

    if (this.plugboard) {
      return Alphabet.convertIndexToCharacter(this.plugboard.sendInput(finalResult));
    }

    return Alphabet.convertIndexToCharacter(finalResult);
  }

  /**
   * Increse the first rotor, if it made a whole rotation
   * rotate the second one aswell. If this one rotated once
   * increase the position of the third one
   */
  private increaseRotorPositions(): void {
    if (this.rotors[0].increasePosition()) {
      if (this.rotors[1].increasePosition()) {
        this.rotors[2].increasePosition();
      }
    }
  }

  /**
   * Decrease the first rotor, if it made a whole rotation
   * rotate the second one aswell. If this one rotated once
   * decrease the position of the third one
   */
  public decreaseRotorPositions(): void {
    if (this.rotors[0].decreasePosition()) {
      if (this.rotors[1].decreasePosition()) {
        this.rotors[2].decreasePosition();
      }
    }
  }
}
