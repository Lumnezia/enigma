const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ BACKSPACE";

let enigma;

window.addEventListener("load", () => {
  const output = document.getElementById("char");
  
  document.addEventListener("keydown", (e) => {
    //Ignore all keys except letter, spacebar and backspace
    if (alphabet.search(e.key.toUpperCase()) === -1 || enigma === undefined) return false;

    if (e.key === " ") {
      output.innerHTML += " ";
    } else if (e.key === "Backspace") {
      goBack();
    } else {
      output.innerHTML += enigma.encryptCharacter(e.key);
    }
    displayRotorPositions();
  });

  document.getElementById("reinit").addEventListener("click", (e) => {

    output.innerHTML = " ";

    let r1Select = document.getElementById("rotor_1") as HTMLSelectElement;
    let r2Select = document.getElementById("rotor_2") as HTMLSelectElement;
    let r3Select = document.getElementById("rotor_3") as HTMLSelectElement;
    let ukwSelect = document.getElementById("ukw_1") as HTMLSelectElement;

    let r1Pos = document.getElementById("rotor_1_pos") as HTMLInputElement;
    let r2Pos = document.getElementById("rotor_2_pos") as HTMLInputElement;
    let r3Pos = document.getElementById("rotor_3_pos") as HTMLInputElement;

    let plugboard = new Plugboard();

    initEnigma(
      ROTOR[r1Select.value],
      parseInt(r1Pos.value),
      ROTOR[r2Select.value],
      parseInt(r2Pos.value),
      ROTOR[r3Select.value],
      parseInt(r3Pos.value),
      UKW[ukwSelect.value],
      plugboard
    );
  });

  document.getElementById("reinit").click();
});

/**
 * Will create the enigma using the given settings
 * @param r1 The rotor to use in the primary slot
 * @param r1Pos The position of the primary rotor
 * @param r2 The rotor to use in the secondary slot
 * @param r2Pos The position of the secondary rotor
 * @param r3 The rotor to use in the tertiary slot
 * @param r3Pos The position of the tertiary rotor
 * @param UKW The ukw to use in the enigma
 * @param plugboard The plugboard to use
 */
const initEnigma = (r1: ROTOR, r1Pos: number, r2: ROTOR, r2Pos: number, r3: ROTOR, r3Pos: number, UKW: UKW, plugboard: Plugboard) => {
  let rotor1 = new Rotor(r1, r1Pos);
  let rotor2 = new Rotor(r2, r2Pos);
  let rotor3 = new Rotor(r3, r3Pos);
  let ukw = new Ukw(UKW);
  let pb = plugboard;
  enigma = new Enigma([rotor1, rotor2, rotor3], ukw, pb);
};

/**
 * Remove the last character from the current text.
 * If the last character is no space, will also decrease
 * the rotor positions
 */
const goBack = () => {
  const output = document.getElementById("char");
  const currentText = output.innerHTML;

  if (currentText.length > 0) {
    output.innerHTML = currentText.slice(0, currentText.length - 1);
    if (currentText[currentText.length - 1] != " ") {
      enigma.decreaseRotorPositions();
    }
  }
};
