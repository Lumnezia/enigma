enum UKW {
  I = 0,
  II = 1,
  III = 2,
}

/**
 * Official wirings of the german enigma machine. The last one is used for testing and is wired one-to-one
 */
const UKW_WIRINGS = [
  [4, 9, 12, 25, 0, 11, 24, 23, 21, 1, 22, 5, 2, 17, 16, 20, 14, 13, 19, 18, 15, 8, 10, 7, 6, 3],
  [24, 17, 20, 7, 16, 18, 11, 3, 15, 23, 13, 6, 14, 10, 12, 8, 4, 1, 5, 25, 2, 22, 21, 9, 0, 19],
  [5, 21, 15, 9, 8, 0, 14, 24, 4, 3, 17, 25, 23, 22, 6, 2, 19, 10, 20, 16, 18, 1, 13, 12, 7, 11],
];

class Ukw {
  private wiring: number[];
  private name: string;

  /**
   * Will create a new ukw and set the name and wiring
   * @param wiring The wiring to use
   */
  constructor(wiring: UKW) {
    this.wiring = UKW_WIRINGS[wiring];
    this.name = UKW[wiring];
  }

  /**
   * Will return the name of the ukw
   */
  public getName(): string {
    return this.name;
  }

  /**
   * Will return a converted character based on the wiring
   * @param char The character to convert
   */
  public sendInput(char: number): number {
    return this.wiring[char];
  }
}
