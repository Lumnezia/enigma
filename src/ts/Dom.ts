const displayRotorPositions = () => {
  /*document.getElementById("rotor_1_position").innerHTML = rotor1.getPosition().toString();
  document.getElementById("rotor_2_position").innerHTML = rotor2.getPosition().toString();
  document.getElementById("rotor_3_position").innerHTML = rotor3.getPosition().toString();*/
};

window.addEventListener("load", (e) => {
  /*document.getElementById("rotor_1_name").innerHTML = rotor1.getName();
  document.getElementById("rotor_2_name").innerHTML = rotor2.getName();
  document.getElementById("rotor_3_name").innerHTML = rotor3.getName();
  document.getElementById("ukw_name").innerHTML = ukw.getName();
  displayRotorPositions();*/

  if (document.hasFocus()) {
    document.querySelector(".output__cursor").classList.add("output__cursor--focus");
  }

  document.onfocus = (e) => {
    document.querySelector(".output__cursor").classList.add("output__cursor--focus");
  };

  document.onblur = () => {
    document.querySelector(".output__cursor").classList.remove("output__cursor--focus");
  };
});
