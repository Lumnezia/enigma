enum ROTOR {
  I = 0,
  II = 1,
  III = 2,
  IV = 3,
  V = 4,
  TEST = 5,
}

/**
 * Official wirings of the german enigma machine. The last one is used for testing and is wired one-to-one
 */
const ROTOR_WIRINGS = [
  [4, 10, 12, 5, 11, 6, 3, 16, 21, 25, 13, 19, 14, 22, 24, 7, 23, 20, 18, 15, 0, 8, 1, 17, 2, 9],
  [0, 9, 3, 10, 18, 8, 17, 20, 23, 1, 11, 7, 22, 19, 12, 2, 16, 6, 25, 13, 15, 24, 5, 21, 14, 4],
  [1, 3, 5, 7, 9, 11, 2, 15, 17, 19, 23, 21, 25, 13, 24, 4, 8, 22, 6, 0, 10, 12, 20, 18, 16, 14],
  [4, 18, 14, 21, 15, 25, 9, 0, 24, 16, 20, 8, 17, 7, 23, 11, 13, 5, 19, 6, 10, 3, 2, 12, 22, 1],
  [21, 25, 1, 17, 6, 8, 19, 24, 20, 15, 18, 3, 13, 7, 11, 23, 0, 22, 12, 9, 16, 14, 5, 4, 2, 10],
  [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25],
];

class Rotor {
  private wiring: number[];
  public position: number;
  private name: string;

  /**
   * Create a new rotor, set the wiring and starting position
   * @param wiring The wiring to use
   * @param position The starting position of the rotor
   */
  constructor(wiring: ROTOR, position: number) {
    this.wiring = ROTOR_WIRINGS[wiring];    
    this.position = position;
    this.name = ROTOR[wiring];
  }

  /**
   * Will convert a character forwards
   * based on position and wiring
   * @param char The character to convert
   */
  public sendForwardInput(char: number): number {
    return this.wiring[(char + this.position) % 26];
  }

  /**
   * Will convert a character backwards
   * based on position and wiring
   * @param char The character to convert
   */
  public sendBackwardsInput(char: number): number {
    for (let i = 0; i < 26; i++) {
      if (char === this.wiring[i]) {
        let output = (i - this.position + 26) % 26;
        return output;
      }
    }
  }

  /**
   * Increase the position of the rotor. If a whole
   * revolution occured, the method will return true
   * false otherwise
   */
  public increasePosition(): boolean {
    this.position = (this.position + 1) % 26;
    return this.position === 0;
  }

  /**
   * Decrease the position of the rotor. If a whole
   * revolution occured, the method will return true
   * false otherwise
   */
  public decreasePosition(): boolean {
    this.position = (this.position - 1 + 26) % 26;
    return this.position === 25;
  }

  /**
   * Return the name of the Rotor
   */
  public getName(): string {
    return this.name;
  }

  /**
   * Return the current position of the rotor
   */
  public getPosition(): number {
    return this.position;
  }
}
