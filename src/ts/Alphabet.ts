class Alphabet {
  public static convertCharacterToIndex(letter: string): number {
    if (letter.length !== 1) return -1;
    const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    return alphabet.search(letter.toUpperCase());
  }

  public static convertIndexToCharacter(index: number): string {
    if (index < 0 || index > 25) return null;
    const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    return alphabet[index];
  }
}
